.globl lat
lat:
	mov x5, 24
	mov x17, x30
	mov x15,5
Tloop:
	mov w7, 0xF800			// Color, rojo
	mov x3, 327				// X position
	mov X4, 0 				// Y position
	bl dibujaT

	sub x16, x1, 4
	mov x10, 2
	mul x10, x5, x10
	sub x16 , x16, x10
	cmp x15, x16				// Chequea si pisa el borde de abajo
    b.eq TretLink

  	mov x10,0x3EEE			  
    bl delayLoop

	add x15, x15, 1

	b Tloop

dibujaT:
	mov x18, x30

	mov x16, x5
	mov x6, x5 				// altura = ancho
	mov x10, 3
	mul x5, x5, x10			// ancho de la cabeza = 3*ancho de un cuadrado

	add x4, x4 ,x15 		// agrego el offset

	bl dibujaCuadrado

	sub x4, x4, 1
	bl drawBlack
	add x4, x4, 1

	mov x5, x16				// reestablesco el ancho
	mov x6, x5
	add x3, x3, x5			// Nueva X = Vieja X + Ancho
	add x4, x4, x5 			// Nueva Y = Vieja Y + Ancho

	bl dibujaCuadrado

	ret x18

TretLink:
	ret x17
