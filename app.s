.globl app
app:
	// X0 contiene la direccion base del framebuffer
	// X3 X de la esquina superior izquierda de rectangulo
	// X4 Y de la esquina superior izquierda de rectangulo
	// X5 ancho del rectángulo
	// X6 alto del rectángulo
	// W7 color a pintar
	// Tamaño X = 512
	// Tamaño Y = 512
	// x18 registro stack USAR CON CUIDADO !!!!!!!1111!!uno!1111!!!111!!!
	//---------------- CODE HERE ------------------------------------

	mov x1, 512
	mov x3, 83				// X del inicio del Gran Rectangulo Blanco (GRB)
	mov x4, 0 				// Y del GRB
	mov x5, 344				// ancho del GRB
	mov x6, 512				// alto del GRB
	mov w7, 0xFFFF			// color del GRB (Blanco! vaya sorpresa)
	mov	x15, 0 				// Offset
	bl dibujaCuadrado		// Dibuja un rectangulo blanco gigante
InfLoop:
	mov x3, 87				// X del No Tan Gran Rectangulo Negro (NTGRN)
	mov x4, 4 				// Y del NTGRN
	mov x5, 336				// ancho del NTGRN
	mov x6, 504				// alto del NTGRN
	mov w7, 0x0000			// Color del NTGRN (Negro!)
	bl dibujaCuadrado		//Dibuja un rectangulo negro, dentro del blanco. Para hacer los bordes

    mov x5, 24          	// Ancho de todos los cuadrados que componen las figuras, cada figura estaria compuesta por 4 cuadrados.
    mov x15, 5 				// offset para dibujar dentro del borde

	bl laL
	bl elPalito
	bl laZ
	bl lat
	bl elCuadrado
	bl resetLine
	mov x10, 0xFFFFF
	bl delayLoop


	b InfLoop


drawBlack:
	mov x19, x30			// guarda return en pseudostack
	mov x2, 0
	mov w2, w7				// guarda color
	mov w7, 0x0000			// negro
	mov x6, 1				// rectangulo negro 1 de alto
	bl dibujaCuadrado
	mov w7, w2
	ret x19

dibujaCuadrado:
    mov x10, 0      		//Columna
    mov x11, 0      		//Fila
    mov x12, x3    			// X local
    mov x13 , x4    		// Y local
    b consiguePixel

consiguePixel:
    lsl x9, x13, 9    		// x9 = 512 * x13 (direccion Y)
    add x9, x12, x9    		// x9 = 512 * (direccion Y) + x12 (Direccion X)
    lsl x9, x9, 1      		// x9 = 2*(512 * (direccion Y) + (Direccion X))
    add x9, x0, x9     		// direccion pixel(x,y) = direccion base + 2*(512*Y + X)
    b rowPaint

rowPaint :
        sturh w7,[x9] 		// pixel x9 tienen el color de w7
        add x9, x9, 2 		// Next Pixel (1 pixel = 2 bytes)
        add x10,x10,1 		// Next column
        sub x14, x10, x5
        cbnz x14, rowPaint  // checkea si se termino la fila, alcanzando el ancho de x5
        b rowJump           // una vez que termino, cambia de fila

rowJump:					// - Salta a la siguiente fila
    mov x10,0               // Resetea la fila
    add x11,x11, 1          // Avanza una fila
    add x13, x13, 1         // El Y aumenta
    sub x14, x11, x6
    cbnz x14, consiguePixel // Se checkea si no se alcanzo la altura deseada, la de x6, sino se actualiza el pixel con la altura nueva de Y
    br x30                  // Regresa al punto de llamada

delayLoop:
	sub x10,x10,1
	cbnz x10,delayLoop
	br x30


//---------------------------------------------------------------

        // Infinite Loop
