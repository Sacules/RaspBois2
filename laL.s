.globl laL


laL:

	mov x5, 24
	mov x17, x30
	mov x15, 5
Lloop:
	mov w7, 0x07E0			// Color, creo que es verde
 	mov x3, 399				// X position
	mov x4, 0				// Y position
	bl dibujaL
	mov x5, 24

	sub x16, x1, 4
	mov x10, 3
	mul x10, x5, x10
	sub x16 , x16, x10
	cmp x15, x16				// Chequea si pisa el borde de abajo
    b.eq LretLink

    mov x10,0x3EEE
    bl delayLoop

 	add x15, x15, 1

 	b Lloop

dibujaL:
	mov x18, x30

	mov x10, 3
	mul x6, x5, x10 		// altura = 3 veces el ancho

	add x4, x15, x4			// Agrego el offset

    bl dibujaCuadrado		// dibuja el rectangulo grande de la L (el palito)

	sub x4, x4, 1			// así no borra el rectangulo antes de dibujarlo
	bl drawBlack
	add x4, x4, 1

    sub x3, x3, x5      	// Nueva X = Vieja X - ancho
    mov x10, 2
    mul x10, x5 , x10
    add x4, x4, x10 		// Nueva Y = Vieja Y + 2 * ancho
    mov x6, x5          	// Nueva altura = ancho

    bl dibujaCuadrado		// Dibuja la patita de la L, pero espejada

	sub x4, x4, 1
	bl drawBlack
	add x4, x4, 1
    ret x18

 LretLink:
 	ret x17
